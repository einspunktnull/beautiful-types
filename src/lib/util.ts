import {Composite} from './base';
import {MapAllTo, MapByTo, RejectBy} from "./properties";

export type Empty<T extends Composite> = MapAllTo<T, never>;
// export type Without<T extends Composite, U extends Composite> = T & Empty<U>;
export type Without<T extends Composite, U extends Composite> = RejectBy<T, keyof U>
export type Extend<T extends Composite, U extends Composite> = MapByTo<T, keyof U, keyof U>
