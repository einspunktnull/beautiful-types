import {Composite} from './base';

export type HasSame<T1, T2> = keyof T1 extends keyof T2 ? true : false;

// "change all properties' types"
// set types of all properties to NewPropType
export type MapAllTo<Source extends Composite, NewPropType>
    = { [Key in keyof Source]?: NewPropType; };

// "change matching properties's types and keep the other's"
// set types of all properties, which are type of ConditionPropType to NewPropType
// set types of all properties, which are not type of ConditionPropType to type of property of origin
export type MapByTo<Source extends Composite, ConditionPropType, NewPropType> =
    { [Key in keyof Source]?: Source[Key] extends ConditionPropType ? NewPropType : Source[Key]; };

// "change not matching properties's types and keep the other's"
// set types of all properties, which are not type of ConditionPropType to NewPropType
// set types of all properties, which are type of ConditionPropType to type of property of origin
export type MapByNotTo<Source extends Composite, ConditionPropType, NewPropType> =
    { [Key in keyof Source]?: Source[Key] extends ConditionPropType ? Source[Key] : NewPropType; };

// "remove all properties"
// set types of all properties, which are not type of ConditionPropType to never
export type FilterBy<Source extends Composite, ConditionPropType>
    = { [Key in keyof Source]?: Source[Key] extends ConditionPropType ? Source[Key] : never; };

// set types of all properties, which are type of ConditionPropType to never
export type RejectBy<Source extends Composite, ConditionPropType>
    = { [Key in keyof Source]?: Source[Key] extends ConditionPropType ? never : Source[Key]; };

// "change matching properties's types and remove other properties"
// set types of all properties, which are not type of ConditionPropType to never
// set types of all properties, which are type of ConditionPropType to NewPropType
export type FilterByAndMapTo<Source extends Composite, ConditionPropType, NewPropType>
    = { [Key in keyof Source]?: Source[Key] extends ConditionPropType ? NewPropType : never };

// "change non matching properties's types and remove other properties"
// set types of all properties, which are type of ConditionPropType to never
// set types of all properties, which are not type of ConditionPropType to NewPropType
export type RejectByNotAndMapTo<Source extends Composite, ConditionPropType, NewPropType>
    = { [Key in keyof Source]?: Source[Key] extends ConditionPropType ? never : NewPropType };
