import {Or} from './base';

// callback stuff
export type Callback = (...args: any[]) => void;
export type RichCallback<T> = (data: T, ...args: any[]) => void;
export type ErrorCallback = (error: any, ...args: any[]) => void;
export type RichErrorCallback<T> = (error: any, data: T, ...args: any[]) => void;

// promise stuff
export type OrPromise<T> = Or<T, Promise<T>>;
export type Resolve<T> = (value: T | PromiseLike<T>) => void;
export type Reject = (reason?: any) => void;
export type AsyncFct<T, Targs = any> = (...args: Targs[]) => Promise<T>;
