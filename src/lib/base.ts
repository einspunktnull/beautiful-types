export type Key = string | number | symbol;
export type Composite = object;
export type Bin = Composite;
export type And<T, U> = T & U;
export type Or<T, U> = T | U;
export type Self<S> = S;
export type OneOrMany<T> = T | Array<T>;
export type Multiple<T> = OneOrMany<T>; // alias
