import {Primitive} from 'type-fest';
import {Bin, Composite, Key} from './base';

type NestArray<T, K extends Key = Key> = Array<Nest<T, K>>;
type NestMap<T, K extends Key = Key> = { [key in K]?: Nest<T, K>; };
type NestBase<T, K extends Key = Key> = T | NestArray<T, K> | NestMap<T, K>;

export type Nest<T, K extends Key = Key> = NestBase<T, K>;
export type NonPrimitveNest<T extends Composite, K extends Key = Key> = Nest<T, K>;
export type PrimitiveNest<T extends Primitive, K extends Key = Key> = Nest<T, K>;
export type BinNest = Nest<Bin>;

