import {Primitive} from 'type-fest';

export type BooleanArray = Array<boolean>;
export type StringArray = Array<string>;
export type NumberArray = Array<number>;
export type SymbolArray = Array<symbol>;
export type BigintArray = Array<bigint>;
export type PrimitveArray = Array<Primitive>;
export type NeverArray = Array<never>;

export type NonZeroSizeArray<Num extends number, T> = {
    0: T;
    length: Num;
} & ReadonlyArray<T>;
export type FixedSizeArray<Num extends number, T> = Num extends 0 ? NeverArray : NonZeroSizeArray<Num, T>;
export type Size2Array<T> = [T, T];
