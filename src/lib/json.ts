import {Nest} from './nest';
import {JsonPrimitive} from 'type-fest';

export type JsonKey = string;
export type Json = Nest<JsonPrimitive, JsonKey>;
