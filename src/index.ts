export * from 'type-fest';
export * from './lib/array';
export * from './lib/async';
export * from './lib/base';
export * from './lib/json';
export * from './lib/nest';
export * from './lib/properties';
export * from './lib/util';

//     https://gist.github.com/mstn/5f75651100556dbe30e405691471afe3
// //https://stackoverflow.com/a/63918062
//         type PrependNextNum<A extends Array<unknown>> = A['length'] extends infer T ? ((t: T, ...a: A) => void) extends ((...x: infer X) => void) ? X : never : never;
//     type EnumerateInternal<A extends Array<unknown>, N extends number> = { 0: A, 1: EnumerateInternal<PrependNextNum<A>, N> }[N extends A['length'] ? 0 : 1];
//     export type Enumerate<N extends number> = EnumerateInternal<[], N> extends (infer E)[] ? E : never;
//     export type Range<FROM extends number, TO extends number> = Exclude<Enumerate<TO>, Enumerate<FROM>>;
//
//     type Uwe = {
//         // [P in keyof [] as `p_${P}`]:'uwe';
//         [N in Enumerate<5> as `p[${N}]`]?: 'uwe';
//     }
//     const uwe: Uwe = {
//         // prop1:'uwe'
//         'p[2]': 'uwe'
//     };


